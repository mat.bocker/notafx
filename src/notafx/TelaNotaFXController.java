/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package notafx;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

/**
 *
 * @author Aluno
 */
public class TelaNotaFXController implements Initializable {
    
    @FXML
    private Button addAluno;
    @FXML
    private Button calcTri;
    @FXML
    private TextField nomeAluno;
    @FXML
    private TextField nota1Prog1;
    @FXML
    private TextField nota2Prog1;
    @FXML
    private TextField nota3Prog1;
    @FXML
    private TextField nota1Prog2;
    @FXML
    private TextField nota2Prog2;
    @FXML
    private TextField nota3Prog2;
    @FXML
    private TextField nota1Calc;
    @FXML
    private TextField nota2Calc;
    @FXML
    private TextField nota3Calc;
    @FXML
    private Label nota1Saida;
    @FXML
    private Label nota2Saida;
    @FXML
    private Label nota3Saida;
    @FXML
    private Label mediaSaida;
    @FXML
    private TextField nomeComp;
    @FXML
    private Button comparar;
    @FXML
    private Button listar;
    
    
    @FXML
    private void handleButtonAction(ActionEvent event) {
        
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }  
    Turma alunos = new Turma();
    @FXML
    private void addAluno()
    {
        alunos.adicionarAluno(nomeAluno.getText(),Integer.parseInt(nota1Prog1.getText()),Integer.parseInt(nota2Prog1.getText()),Integer.parseInt(nota3Prog1.getText()),Integer.parseInt(nota1Prog2.getText()),Integer.parseInt(nota2Prog2.getText()),Integer.parseInt(nota3Prog2.getText()));
    }
    @FXML
    private void calcTri()
    {
        double resp;
        resp=alunos.calcularTrimestre(Integer.parseInt(nota1Calc.getText()),Integer.parseInt(nota2Calc.getText()),Integer.parseInt(nota3Calc.getText()));
        nota1Saida.setText("Nota 1: "+nota1Calc.getText());
        nota2Saida.setText("Nota 2: "+nota2Calc.getText());
        nota3Saida.setText("Nota 3: "+nota3Calc.getText());
        mediaSaida.setText("Media: "+resp);
    }
    @FXML
    private void comparar()
    {
        alunos.comp(nomeComp.getText());
    }
    @FXML
    private void listar()
    {
        alunos.mostrarTudo();
    }
    @FXML
    private void limpaCadastro()
    {
        nomeAluno.setText("");
        nota1Prog1.setText("");
        nota2Prog1.setText("");
        nota3Prog1.setText("");
        nota1Prog2.setText("");
        nota2Prog2.setText("");
        nota3Prog2.setText("");
    }
    @FXML
    private void limpaCalculo()
    {
        nota1Calc.setText("");
        nota2Calc.setText("");
        nota3Calc.setText("");
    }
    @FXML
    private void limpaComp()
    {
        nomeComp.setText("");
    }
}
