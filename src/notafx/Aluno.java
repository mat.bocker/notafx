/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package notafx;

/**
 *
 * @author Aluno
 */
public class Aluno {
    private String nome;
    private double[] Nota1 = new double[3];
    private double[] Nota2 = new double[3];
    private double media;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public double getNota1(int i) {
        return Nota1[i];
    }

    public void setNota1(double Nota1,int i) {
        this.Nota1[i] = Nota1;
    }

    public double getNota2(int i) {
        return Nota2[i];
    }

    public void setNota2(double Nota2,int i) {
        this.Nota2[i] = Nota2;
    }

    public double getMedia() {
        return media;
    }

    public void setMedia(double media) {
        this.media = media;
    }
    
}
